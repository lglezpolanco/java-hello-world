package day_1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class program
{
    public static void main(String[] args)
    {
        System.setProperty("webdriver.chrome.driver","D:\\Software\\Selenium\\Chromedriver_win32\\chromedriver_win32\\chromedriver.exe"); //poner la ruta de acceso
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize(); //abriendo y maximizando la ventana del navegador
        driver.get("http://google.com"); //URL a probar
        System.out.println(driver.getTitle()); //Debería obtener "Google"
        driver.quit(); //para cerrar el navegador

    }

}
